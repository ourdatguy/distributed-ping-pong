# README #

The purpose of this application is to demo a distributed system using a ping pong example.

The system is made up of three main parts:
* Client - Connects to the system and sends "pings" so it can receive "pongs"
* Matcher - responsible for collecting clients and pairing them together and routing all messages from clients to the appropriate Keeper.
* Keeper - Keeps track of paired Clients, stores the score between the clients, and contains logic to determine whether or not a Client misses a ping (each ping is hardcoded to have a 10% chance of being missed).

Note that this is not the most effective way to model an actual ping-pong game between two clients, but rather was implemented this way to model a more complex system, namely the one described in the Typesafe Webinar [Mobile and Omnichannel Environments | https://www.youtube.com/watch?t=388&v=oXywS0gPN84].

### Intended Functionality ###

Yes, the word "intended" is used to denote that this is still a work in progress and is actually very buggy and does not have all features implemented yet.

The intended functionality is that you should be able to start up multiple nodes on a server and connect Clients to those nodes. Because of Akka Clustering, if one of the nodes goes down, the clients should still be able to recover. Because of Akka Sharding, we can have multiple Matchers in the system, avoiding a single point of failure. Because of Akka Sharding, Matchers can direct messages to the correct Keeper without having to maintain any state. Finally, With Akka Persistence, if a node containing state goes down, we are able to restore that state and the client can continue as normal.

### How do I get set up? ###

The repository is split into two separate directories, akka-client and akka-server. To set up the cluster environment use the following instructions.

1. Start the server
    1. In akka-server, run `activator -Dakka.remote.netty.tcp.port=9090 -Dakka.cluster.roles.0=matcher -Dakka.cluster.roles.1=shard run` This will start up a single instance of the server.
    2. In another terminal run the same command but change the port to `9090` to `9091`. `9090` and `9091` are the ports of the seed nodes for the cluster, and at least the first node of the cluster always needs to be started on one of these two ports.
    3. Add additional nodes to the cluster by setting the value of the port to `0`, this tells Akka to automatically choose an available port.
2. Start the clients
    1. In multiple terminal windows, navigate to the akka-client directory and run `activator run`.
    2. You will need to keep making clients until at least two of them connect in one of the nodes.

### Known Issues ###

I'm sharing this code for reference to those who attended the presentation this code was written for, but the code itself is very buggy and really only works part of the time. Although the concepts behind it's inception are accurate, it appears that there are configurations that are missing. Known issues are:

* Akka Persistence is not working for Keeper actors. As such, if the node that the Keeper is shut down, clients are not able to recover.
* Because Keepers are not persistent, when testing the resiliency of the cluster you need to stop only the node that is running the Matcher that the clients are connected on. The Matcher can by identified because during the volley it will output `"ping received from ${sender.path}"`, all other output comes from the Keepers. Unfortunately, because Matcher nodes are selected at random and Keeper nodes are chosen by hashing a timestamp, it is completely by chance that the two services are not actually running on the same node.
* It appears that `shard` roles are not visible to the cluster unless accompanied by a `matcher` role. If one node is created with only the role `matcher` and another node is created with only the role `shard`, Keeprs will never be created as the `matcher` node does not have the role, and the `shard` node seems to not be visible to the cluster. As such, all nodes currently need to have both the `matcher` and `shard` roles.
* Finally, if all conditions above just happen to be met and the Matchers and Keepers are running on separate nodes, it seems that the clients can only recover if the node the Matcher is running on (i.e. the one that is killed for this example) is NOT the cluster leader.

### Contribution guidelines ###

Feel free to fork and fix any of the above known issues if you are interested. When fixing an issue be sure to remove it from the known issues list above, and update the documentation for any configurations that need to be passed in from the command line.