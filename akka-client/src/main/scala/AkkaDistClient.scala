import akka.actor.{ ActorRef, ActorSystem, Props, Actor, Inbox }
import scala.concurrent.duration._
import akka.contrib.pattern.ClusterClient
import akka.pattern.ask
import akka.util.Timeout
import akka.pattern.AskTimeoutException

case object Greet
case class WhoToGreet(who: String)
case class Greeting(message: String)

object AkkaDistClient extends App {

  // Create the 'helloakka' actor system
  val system = ActorSystem("testClient")
  
  val initialContacts = Set(
    system.actorSelection("akka.tcp://distTest@127.0.0.1:9090/user/receptionist"),
    system.actorSelection("akka.tcp://distTest@127.0.0.1:9091/user/receptionist")
  )
  val clusterClient = system.actorOf(ClusterClient.props(initialContacts))

  val client = system.actorOf(Props(new Client(clusterClient)))
  client ! "start"
}

// prints a greeting
class Client(clusterClient: ActorRef) extends Actor {
  
  import context.dispatcher
  implicit val timeout: Timeout = 5 seconds
  
  def receive = {
    case "start" => {
      println("starting")
      clusterClient ! ClusterClient.Send(
          "/user/matcher", 
          ("join", java.lang.System.currentTimeMillis().toLong, self), 
          true
      )
    }
    case ("pong", keeperId: Long) => {
      println("sending ping")
      Thread.sleep(1000)
      ping(keeperId, 0)
    }
    case "win" => println("I won!!!")
    case "lose" => println("I suck")
    case a: Any => println(s"any -> ${a}")
  }
  
  private def ping(keeperId: Long, attempts: Int) {
    if(attempts > 0)
      println(s"attempted $attempts time(s) to connect to server")
    
    val msg = ("ping", keeperId, self)
      val result = clusterClient ? ClusterClient.Send("/user/matcher", msg, true)
      result.onSuccess {
        case msg => //println(s"message received: " + msg)
      }
      
      result.onFailure{
        case _: AskTimeoutException => {
          if(attempts < 3){
            Thread.sleep(3000)
            ping(keeperId, attempts + 1)
          }
          else {
            println("attempted to access server three times. quiting.")
            context stop self
          }
        }
      }
  }
}
  
  