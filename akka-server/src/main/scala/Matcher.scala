import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import akka.cluster.Cluster
import akka.cluster.MemberStatus
import akka.contrib.pattern.ClusterReceptionistExtension
import akka.contrib.pattern.ClusterSharding
import akka.contrib.pattern.ShardRegion.IdExtractor
import akka.contrib.pattern.ShardRegion.ShardResolver

class Matcher() extends Actor {
  
  ClusterReceptionistExtension(context.system).registerService(self)
  
  val cluster = Cluster(context.system)
  
  val extractor: IdExtractor = {
    case ("ping", id: Long, sender: ActorRef) => {
      (id.toString, sender)
    }
    case msg @ (_: ActorRef, _: ActorRef, id: Long) => {
      (id.toString, msg)
    }
  }
  
  val resolver: ShardResolver = {
    case (msg: String, id: Long, ref: ActorRef) => {
      val nodeCount = cluster.state.members.filter({ _.status == MemberStatus.Up}).size
      (id % nodeCount).toString
    }
    case msg @ (_: ActorRef, _:ActorRef, id: Long) => {
      val nodeCount = cluster.state.members.filter({_.status == MemberStatus.Up}).size
      (id % nodeCount).toString
    }
  }
  
  val sharding = ClusterSharding(context.system).start(
      typeName = "Keeper", 
      entryProps = Some(Props(new Keeper())), 
      idExtractor = extractor, 
      shardResolver = resolver
  )
  
  def receive = receiveFirst orElse receiveOther
  
  def receiveFirst: Receive = {
    case ("join", _: Long, sender: ActorRef) => {
      println("first client joined")
      context.become(receiveSecond(sender) orElse receiveOther)
    }
  }
  
  def receiveSecond(first: ActorRef): Receive = {
    case ("join", timestamp: Long, sender: ActorRef) => {
      println("second client joined")
      sharding ! (first, sender, timestamp)
      context.unbecome()
    } 
  }
  
  def receiveOther: Receive = {
    case ("ping", id: Long, ref: ActorRef) => {
      println(s"ping received from ${sender.path}")
      sharding ! ("ping", id, ref)
      sender ! "ok"
    }
    case _: Any => println("FAIL!")
  }
}